package org.eclipse.californium.examples;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.eclipse.californium.elements.exception.ConnectorException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Docs: https://firebase.google.com/docs/firestore/quickstart?hl=pt-br 
 * @author kamila
 * 25/09/2019
 * */

public class FirebaseService {
		  
	private DatabaseReference ref;
	private FirebaseApp app;
	private Firestore db;
	
	public FirebaseService() {
		super();
		try {

			FileInputStream serviceAccount = new FileInputStream("C:\\Users\\t_kamila\\Downloads\\californium-2.0.0-M17\\bluetooth-sensor-reader-firebase-adminsdk-57unu-05aa6937d7.json"); 

			FirebaseOptions options = null;
			try {
				options = new FirebaseOptions.Builder()
				    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
				    .setDatabaseUrl("https://bluetooth-sensor-reader.firebaseio.com")
				    .build();	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			if (FirebaseApp.getApps().isEmpty()) {
				app = FirebaseApp.initializeApp(options);
			}
			
			//db = FirestoreClient.getFirestore();
			FirestoreOptions firestoreOptions = FirestoreOptions
					.getDefaultInstance().toBuilder()
					.setProjectId("bluetooth-sensor-reader")
					.build();
			db = firestoreOptions.getService();
		
			final FirebaseDatabase dataBase = FirebaseDatabase.getInstance();
			
			ref = dataBase.getReference("messages/");
			
			// LISTENER PARA LER ALTERAÇÕES NA REFERÊNCIA getTemperatura
			DatabaseReference getTemperatureRef = dataBase.getReference("buscaTemperatura/");
			getTemperatureRef.addValueEventListener(new ValueEventListener() {

				@Override
				public void onDataChange(DataSnapshot dataSnapshot) {
					String getTemperature = dataSnapshot.getValue().toString();
					System.out.println(dataSnapshot.getValue());
					try {
						// REQUISIÇÃO PARA ARDUINO ENVIAR TEMPERATURA
						MessageCoapClient client = new MessageCoapClient();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}

				@Override
				public void onCancelled(DatabaseError databaseError) {
					System.out.println("The read failed: " + databaseError.getCode());
				}
			});
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.err.println("SYSTEM CONCLUIDA");
				

		
	}
	
	public void writeData(Message msg) throws Exception {
	

		// PEGA REFERENCIA DO CAMINHO A SER ESCRITO NO BANCO
		DatabaseReference messagesRef = ref;
	
		// TRANSFORMA O OBJETO EM UM JSON
		Map<String, Object> messages = new HashMap<>();
		
		// INSERE O DADO NO HASHMAP
		messages.put("temperatura", msg.getTemperatura());

		final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		final Calendar calendar = Calendar.getInstance();
		messages.put("dateTime", df.format(calendar.getTime()));

		DatabaseReference newMessagesRef = messagesRef.push();

		// INSERE O DADO NO REALTIME DATABASE
		newMessagesRef.setValue(messages, new DatabaseReference.CompletionListener() {
			@Override
			public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
				if (databaseError != null) {
					System.out.println("Data could not be saved " + databaseError.getMessage());
				} else {
					System.out.println("Data saved successfully.");
				}
			}
		});

	//	ApiFuture<DocumentReference>future = db.collection("messages").add(messages);
		@SuppressWarnings("unused")
		ApiFuture<WriteResult> result = db.collection("messages").document().set(messages);
		
	}
		
}
