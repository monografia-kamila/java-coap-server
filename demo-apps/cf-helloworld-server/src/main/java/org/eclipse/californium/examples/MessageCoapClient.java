package org.eclipse.californium.examples;

import java.io.IOException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.elements.exception.ConnectorException;

public class MessageCoapClient {

	// static boolean getResponse = false;
	public MessageCoapClient() {
		CoapClient client = new CoapClient("coap://127.0.0.1:5683/temperature"); // Arduino ip and port
		System.out.println("SYNCHRONOUS");
		
		try {
			// synchronous
			String content1;
			content1 = client.get().getResponseText();
			System.out.println("RESPONSE 1: " + content1);

			CoapResponse resp2 = client.post("payload", MediaTypeRegistry.TEXT_PLAIN);
			System.out.println("RESPONSE 2 CODE: " + resp2.getCode() + " - " + resp2.getCode().name());
			System.out.println(Utils.prettyPrint(resp2));
		} catch (ConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}